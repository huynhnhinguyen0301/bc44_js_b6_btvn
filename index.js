function chonKhuVuc(layKhuVuc) {
  var khuVuc = 0;

  if (layKhuVuc == "khu-vuc-a") {
    khuVuc = 2;
  } else if (layKhuVuc == "khu-vuc-b") {
    khuVuc = 1;
  } else if (layKhuVuc == "khu-vuc-c") {
    khuVuc = 0.5;
  } else {
    return 0;
  }
  return khuVuc;
}

function chonDoiTuong(layDoiTuong) {
  if (layDoiTuong == "doi-tuong-1") {
    return 2.5;
  } else if (layDoiTuong == "doi-tuong-2") {
    return 1.5;
  } else if (layDoiTuong == "doi-tuong-3") {
    return 1;
  } else {
    return 0;
  }
}

function tinhTongDiem() {
  var diem1 = document.getElementById("diem1").value * 1;
  var diem2 = document.getElementById("diem2").value * 1;
  var diem3 = document.getElementById("diem3").value * 1;
  var tinhTongDiem = diem1 + diem2 + diem3;
  console.log(tinhTongDiem.toFixed(2));
  return tinhTongDiem;
}

function xetDiemChuan() {
  if (document.querySelector(".khu_vuc").value == 0) {
    alert("chọn Khu vực kìa má !!! ");
    return;
  }
  if (document.querySelector('input[name="doiTuong"]:checked') == null) {
    alert("chọn Đối tượng kìa má !!! ");
    return;
  }

  var diem1 = document.getElementById("diem1").value * 1;
  var diem2 = document.getElementById("diem2").value * 1;
  var diem3 = document.getElementById("diem3").value * 1;
  var diemChuan = document.getElementById("diem-chuan").value * 1;
  var diemTong = tinhTongDiem();

  var layKhuVuc = document.querySelector(".khu_vuc").value;
  var layDoiTuong = document.querySelector(
    'input[name="doiTuong"]:checked'
  ).value;

  var hamChonKhuVuc = chonKhuVuc(layKhuVuc);

  var hamChonDoiTuong = chonDoiTuong(layDoiTuong);
  var tongDiem = 0;

  if (diemTong < diemChuan || diem1 == 0 || diem2 == 0 || diem3 == 0) {
    alert(`rớt nha mậy ${diemTong} `);
    return;
  } else {
    tongDiem = hamChonKhuVuc + hamChonDoiTuong + diemTong;
    console.log("hamChonKhuVuc: ", hamChonKhuVuc);
    console.log("hamChonDoiTuong: ", hamChonDoiTuong);
    console.log("diemTong: ", diemTong);

    console.log("đậu rồi má: tongDiem:", tongDiem);
  }
}

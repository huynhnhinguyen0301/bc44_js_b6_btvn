///-------------BÀI 1: QUẢN LÝ TUYỂN SINH ------------------//
function tinhDiemkhuVuc(layGiaTriKhuVuc) {
  var giaTriKhuVuc = 0;
  if (layGiaTriKhuVuc == "khu-vuc-a") {
    giaTriKhuVuc = 2;
  } else if (layGiaTriKhuVuc == "khu-vuc-b") {
    giaTriKhuVuc = 1;
  } else if (layGiaTriKhuVuc == "khu-vuc-c") {
    giaTriKhuVuc = 0.5;
  } else {
    giaTriKhuVuc = 0;
  }
  return giaTriKhuVuc;
}

function tinhDiemDoiTuong(layGiaTriDoiTuong) {
  if (layGiaTriDoiTuong == "doi-tuong-1") {
    return 2.5;
  } else if (layGiaTriDoiTuong == "doi-tuong-2") {
    return 1.5;
  } else if (layGiaTriDoiTuong == "doi-tuong-3") {
    return 1;
  } else {
    return 0;
  }
}

function tinhDiemTong3Mon(diem1, diem2, diem3) {
  var diemTong3Mon = diem1 + diem2 + diem3;

  return diemTong3Mon;
}

function xetDiemChuan() {
  if (document.querySelector(".khu_vuc").value == 0) {
    alert("Chọn khu vực kìa mày !!!");
    return;
  }
  if (document.querySelector('input[name="doiTuong"]:checked') == null) {
    alert("Chọn đối tượng kìa má !!!");
    return;
  }

  var diemChuan = document.getElementById("diem-chuan").value * 1;
  var diem1 = document.getElementById("diem1").value * 1;
  var diem2 = document.getElementById("diem2").value * 1;
  var diem3 = document.getElementById("diem3").value * 1;
  var diemTong3Mon = tinhDiemTong3Mon(diem1, diem2, diem3);
  var layGiaTriKhuVuc = document.querySelector(".khu_vuc").value; // thực ra lấy string để so sánh, nên không cần convert to number
  var layGiaTriDoiTuong = document.querySelector(
    'input[name="doiTuong"]:checked'
  ).value;

  var diemKhuVuc = tinhDiemkhuVuc(layGiaTriKhuVuc);
  var diemDoiTuong = tinhDiemDoiTuong(layGiaTriDoiTuong);
  var tongDiem = 0;

  if (diemChuan > diemTong3Mon || diem1 == 0 || diem2 == 0 || diem3 == 0) {
    document.getElementById("result-diem").innerHTML = `<h3>Rớttttttt</h3>`;

    return;
  } else {
    console.log("Đậu nha");
    tongDiem = diemKhuVuc + diemDoiTuong + diemTong3Mon;
  }
  document.getElementById("result-diem").innerHTML = `<h3>Đậu nha !!!</h3>
  <h3>điểm 1: ${diem1}đ</h3>
  <h3>điểm 2: ${diem2}đ</h3>
  <h3>điểm 3: ${diem3}đ</h3>
  <h3>điểm khu vực:+${diemKhuVuc}đ</h3>
  <h3>điểm đối tượng:+${diemDoiTuong}đ</h3>
  <h3>Điểm 3 môn nè: ${tongDiem}đ</h3>`;
}

////-----------------------BÀI 2: TÍNH TIỀN ĐIỆN -----------------/////

function tinh50KwDauTien() {
  return 500;
}

function tinh50KwĐen100Kw() {
  return 650;
}

function tinh100KwĐen200Kw() {
  return 850;
}

function tinh200KwĐen350Kw() {
  return 1100;
}
function tinhSoKwConLai() {
  return 1300;
}

function tinhTienDien() {
  var hoTen = document.getElementById("ho-ten").value;
  var soKw = document.getElementById("so-kw").value * 1;
  var giaTien50KwDauTien = tinh50KwDauTien();
  var giaTien50KwĐen100Kw = tinh50KwĐen100Kw();
  var giaTien100KwĐen200Kw = tinh100KwĐen200Kw();
  var giaTien200KwĐen350Kw = tinh200KwĐen350Kw();
  var giaTienSoKwConLai = tinhSoKwConLai();

  if (hoTen == 0) {
    alert("dien ho ten");
    return;
  }

  if (soKw == 0) {
    alert("dien so kw");
    return;
  }

  var tongTien = 0;

  if (soKw <= 50) {
    tongTien = tinh50KwDauTien() * soKw;
  } else if (soKw <= 100) {
    tongTien = tinh50KwDauTien() * 50 + tinh50KwĐen100Kw() * (soKw - 50);
  } else if (soKw <= 200) {
    tongTien =
      tinh50KwDauTien() * 50 +
      tinh50KwĐen100Kw() * 50 +
      tinh100KwĐen200Kw() * (soKw - 100);
  } else if (soKw <= 350) {
    tongTien =
      tinh50KwDauTien() * 50 +
      tinh50KwĐen100Kw() * 50 +
      tinh100KwĐen200Kw() * 100 +
      tinh200KwĐen350Kw() * (soKw - 200);
  } else {
    tongTien =
      tinh50KwDauTien() * 50 +
      tinh50KwĐen100Kw() * 50 +
      tinh100KwĐen200Kw() * 100 +
      tinh200KwĐen350Kw() * 150 +
      tinhSoKwConLai() * (soKw - 350);
  }

  console.log(tongTien);

  document.getElementById(
    "result-tien-dien"
  ).innerHTML = `<h3>ho va ten: ${hoTen}</h3>
<h3>Tong tien : ${tongTien.toLocaleString(3)}đ</h3>`;

  //   if (soKw <= 50) {
  //     var tongTien = giaTien50KwDauTien;
  //     console.log("giaTien50KwDauTien", tongTien);
  //   }
}

///------------------BÀI 3: TÍNH THUẾ THU NHẬP CÁ NHÂN -------------------------////

function tinhTienThue() {
  var hoTen = document.querySelector("#ho-ten").value;
  var tongThuNhap = document.querySelector("#thu-nhap").value * 1;
  var soNguoiPhuThuoc = document.querySelector("#nguoi-phu-thuoc").value * 1;

  var thuNhapChiuThue = tongThuNhap - 4000000 - soNguoiPhuThuoc * 1600000;

  var xuatThue = 0;
  if (thuNhapChiuThue < 0) {
    xuatThue = 0;
  } else if (thuNhapChiuThue <= 60000000) {
    xuatThue = thuNhapChiuThue * 0.05;
  } else if (thuNhapChiuThue <= 120000000) {
    xuatThue = thuNhapChiuThue * 0.1;
  } else if (thuNhapChiuThue <= 210) {
    xuatThue = thuNhapChiuThue * 0.15;
  } else if (thuNhapChiuThue <= 384) {
    xuatThue = thuNhapChiuThue * 0.2;
  } else if (thuNhapChiuThue <= 624) {
    xuatThue = thuNhapChiuThue * 0.25;
  } else if (thuNhapChiuThue <= 960) {
    xuatThue = thuNhapChiuThue * 0.3;
  } else {
    xuatThue = thuNhapChiuThue * 0.35;
  }

  document.querySelector("#result-tien-thue").innerHTML = `
  <h4>Họ và tên là: ${hoTen}</h4>
  <h4>Tổng thu nhập là: ${tongThuNhap.toLocaleString(3)}</h4>
  <h4 >Thu nhập cá nhân cần đóng là: ${xuatThue.toLocaleString(3)}</h4>
  <h4>Số người phụ thuộc là: ${soNguoiPhuThuoc}</h4>`;
}

////-------------------TÍNH TIỀN CÁP ----------------------/////

//xử lý ẩn hiện input kết nối

var loaiKhachHangInput = document.querySelector(".loai-khach-hang");
loaiKhachHangInput.addEventListener("change", function () {
  if (loaiKhachHangInput.value === "nha-dan") {
    // ẩn input số kết nối

    var appearEL = document.querySelector(".appear");
    //appearEL.style.display="none";

    appearEL.classList.add("none");
  }

  if (loaiKhachHangInput.value === "doanh-nghiep") {
    //hiện input số kết nối
    var appearEL = document.querySelector(".appear");
    //appearEL.style.display="block";
    appearEL.classList.remove("none");
  }
});

// xử lý ra kết quả cuối cùng
function tinhTienCap() {
  var maKhachHang = document.querySelector("#ma-khach-hang").value;
  var chonKhachHang = document.querySelector(".loai-khach-hang").value;
  var soKetNoi = document.querySelector("#so-ket-noi").value * 1;
  var kenhCaoCap = document.querySelector("#kenh-cao-cap").value * 1;

  var tienCap = 0;
  var soKetNoiDuoi10 = 0;

  if (chonKhachHang ==="nha-dan") {
    tienCap = 4.5 + 20.5 + kenhCaoCap * 7.5;
   
    
  }
  if (chonKhachHang==="doanh-nghiep") {
    if (soKetNoi <= 10) {
     tienCap = 15 + 75 + kenhCaoCap * 50;
    }
    if (soKetNoi > 10) {
      soKetNoiDuoi10 = 15 + 75 + kenhCaoCap * 50;
      tienCap = (soKetNoi - 10) * 5 + soKetNoiDuoi10;
    }
    console.log("tien cáp doanh nghiệp : ", tienCap);
  

  }

  document.getElementById(
    "result-tien-cap"
  ).innerHTML = `<h4> Mã khách hàng: ${maKhachHang} </h4>
  <h4> Tiền cáp : ${tienCap} </h4>`;

}
